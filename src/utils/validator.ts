import { Joi } from 'celebrate';

const emailValidator = Joi.string().email({ minDomainSegments: 2, tlds: { allow: ['com', 'net', 'es', 'org'] } });

const stringLenghtValidator = (min: number, max: number) =>
  Joi.string()
    .min(min)
    .max(max)
    .required();

const stringAlphaValidator = (min: number, max: number) =>
  Joi.string()
    .min(min)
    .max(max)
    .alphaNum()
    .required();

// const passwordValidator = (min: number = 5, max: number = 15) =>
//   Joi.string().pattern(new RegExp(`/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{${min},${max}}$/`));

//const phoneValidator = Joi.number().pattern(new RegExp('/^[679]{1}[0-9]{8}$/'));

export { emailValidator, stringLenghtValidator, stringAlphaValidator /*passwordValidator*/ /*phoneValidator*/ };
