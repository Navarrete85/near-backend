import { Service, Inject } from 'typedi';
import { IUser } from '../definitions/interfaces/IUser';

@Service()
export default class MailerService {
  constructor(@Inject('nodemailer') private nodemailer) {}

  public async SendWelcomeEmail(email) {
    const transporter = this.nodemailer.createTransport({
      service: 'gmail',
      auth: {
        user: 'near.soporte@gmail.com',
        pass: 'Near_Waiter', // naturally, replace both with your real credentials or an application-specific password
      },
    });

    const mailOptions = {
      from: 'near.soporte@gmail.com',
      to: email,
      subject: 'Invoices due',
      text: 'Dudes, we really need your money.',
    };

    transporter.sendMail(mailOptions, function(error, info) {
      if (error) {
        console.log(error);
      } else {
        console.log('Email sent: ' + info.response);
      }
    });
    return { delivered: 1, status: 'ok' };
  }
  public StartEmailSequence(sequence: string, user: Partial<IUser>) {
    if (!user.email) {
      throw new Error('No email provided');
    }
    // @TODO Add example of an email sequence implementation
    // Something like
    // 1 - Send first email of the sequence
    // 2 - Save the step of the sequence in database
    // 3 - Schedule job for second email in 1-3 days or whatever
    // Every sequence can have its own behavior so maybe
    // the pattern Chain of Responsibility can help here.
    return { delivered: 1, status: 'ok' };
  }
}
