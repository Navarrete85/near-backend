import { Service, Inject } from 'typedi';
import { IAllergen } from '../definitions/interfaces/IAllergen';
import { Document } from 'mongoose';

@Service()
export default class TablesService {
  constructor(@Inject('allergenModel') private allergenModel: Models.AllergenModel, @Inject('logger') private logger) {}

  public async getAll(): Promise<(IAllergen & Document)[]> {
    const allergenList = await this.allergenModel.find();
    if (!allergenList) {
      throw new Error('allergen not found');
    }

    this.logger.silly('Show all allergen!');
    return allergenList;
  }
}
