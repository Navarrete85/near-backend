import { Service, Inject } from 'typedi';
import { ITables } from '../definitions/interfaces/ITable';
import { Document } from 'mongoose';

@Service()
export default class TablesService {
  constructor(@Inject('tablesModel') private tablesModel: Models.TablesModel, @Inject('logger') private logger) {}

  public async getAll(): Promise<(ITables & Document)[]> {
    const tablesList = await this.tablesModel.find();
    if (!tablesList) {
      throw new Error('tables not found');
    }

    this.logger.silly('Show all tables!');
    return tablesList;
  }

  public async demo(): Promise<Object> {
    return {
      content: 'Esto es una prueba',
    };
  }
}
