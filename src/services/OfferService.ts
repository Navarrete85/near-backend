import { Service, Inject } from 'typedi';
import { IOfer } from '../definitions/interfaces/IOffers';
import { Document } from 'mongoose';

@Service()
export default class TablesService {
  constructor(@Inject('offerModel') private offerModel: Models.OfferModel, @Inject('logger') private logger) {}

  public async getAll(): Promise<(IOfer & Document)[]> {
    const offerList = await this.offerModel.find();
    if (!offerList) {
      throw new Error('offer not found');
    }

    this.logger.silly('Show all offer!');
    return offerList;
  }
}
