import { Service, Inject } from 'typedi';
import { IUser } from '../definitions/interfaces/IUser';
import { Document } from 'mongoose';

@Service()
export default class UserService {
  constructor(@Inject('userModel') private userModel: Models.UserModel, @Inject('logger') private logger) {}

  public async getAll(): Promise<(IUser & Document)[]> {
    const usersList = await this.userModel.find();
    if (!usersList) {
      throw new Error('Users not found');
    }

    this.logger.silly('Show all users!');
    return usersList;
  }

  public async getUserByProp(props: any): Promise<IUser & Document> {
    console.error('Esto es el contenido del id --> ', props);
    let user = await this.userModel.findOne(props);
    console.error('Esto es lo que devuelve mongoose --> ', user);

    if (!user) {
      throw new Error(`User with prop ${JSON.stringify(props)} not found`);
    }

    return user;
  }
}
