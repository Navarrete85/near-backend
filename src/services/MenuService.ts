import { Service, Inject } from 'typedi';
import { IMenu } from '../definitions/interfaces/IMenu';
import { Document } from 'mongoose';

@Service()
export default class TablesService {
  constructor(@Inject('menuModel') private menuModel: Models.MenuModel, @Inject('logger') private logger) {}

  public async getAll(): Promise<(IMenu & Document)[]> {
    const menuList = await this.menuModel.find();
    if (!menuList) {
      throw new Error('menu not found');
    }

    this.logger.silly('Show all menu!');
    return menuList;
  }
}
