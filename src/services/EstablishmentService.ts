import { Service, Inject } from 'typedi';
import { IEstablishment } from '../definitions/interfaces/IEstablishment';
import { Document } from 'mongoose';

@Service()
export default class TablesService {
  constructor(
    @Inject('establishmentModel') private establishmentModel: Models.EstablishmentModel,
    @Inject('logger') private logger,
  ) {}

  public async getAll(): Promise<(IEstablishment & Document)[]> {
    const establishmentList = await this.establishmentModel.find();
    if (!establishmentList) {
      throw new Error('establishment not found');
    }

    this.logger.silly('Show all establishment!');
    return establishmentList;
  }
}
