import { Router } from 'express';
import auth from './routes/auth';
import user from './routes/user';
import tables from './routes/tables';
import offer from './routes/offerRoute';
import menu from './routes/MenuRoute';
import establishment from './routes/EstablishmentRoute';
import allergen from './routes/AllergenRoute';
import agendash from './routes/agendash';

// guaranteed to get dependencies
export default () => {
  const app = Router();
  auth(app);
  user(app);
  tables(app);
  offer(app);
  menu(app);
  establishment(app);
  allergen(app);
  agendash(app);

  return app;
};
