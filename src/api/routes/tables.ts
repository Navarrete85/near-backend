import { Router, Request, Response } from 'express';
// import middlewares from '../middlewares';
import TablesService from '../../services/tables';
import { Container } from 'typedi';
// import { ENUMS, ENUMS_MAP } from '../../definitions/enum-hub';
const route = Router();

export default (app: Router) => {
  app.use('/table', route);

  /**
   * @swagger
   * /api/table/all:
   *    get:
   *      description: Get all tables of a stablishment
   *      responses:
   *        200:
   *          descriotion: Success
   */
  route.get('/all', async (req: Request, res: Response) => {
    try {
      let TablesServiceInstance = Container.get(TablesService);
      const allTables = await TablesServiceInstance.getAll();
      return res.status(200).json(allTables);
    } catch (e) {
      return res.status(500).json(e);
    }
  });

  route.get('/prueba', async (req: Request, res: Response) => {
    try {
      let TablesServiceInstance = Container.get(TablesService);
      const result = await TablesServiceInstance.demo();
      return res.status(200).json(result);
    } catch (e) {
      return res.status(500).json(e);
    }
  });
};
