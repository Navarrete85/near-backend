import { Router, Request, Response } from 'express';
import middlewares from '../middlewares';
import UserService from '../../services/user';
import { Container } from 'typedi';
// import { ENUMS, ENUMS_MAP } from '../../definitions/enum-hub';
// import user from '../../models/user';
const route = Router();

export default (app: Router) => {
  app.use('/user', route);

  route.get('/a', (req: Request, res: Response) => {
    return res.json({ user: 'holamundo', state: 'excelente', enviroment: 'Develop2' }).status(200);
  });

  route.get('/me', middlewares.isAuth, middlewares.attachCurrentUser, (req: Request, res: Response) => {
    return res.json({ user: req.currentUser }).status(200);
  });

  route.get('/all', middlewares.isAuth, async (req: Request, res: Response) => {
    try {
      let userServiceInstance = Container.get(UserService);
      const allUsers = await userServiceInstance.getAll();
      return res.status(200).json(allUsers);
    } catch (e) {
      return res.status(500).json(e);
    }
  });

  route.get('/:id', async (req: Request, res: Response) => {
    try {
      let userServiceInstance = Container.get(UserService);
      let result = await userServiceInstance.getUserByProp({ _id: req.params.id });
      res.status(200).json(result);
    } catch (e) {
      return res.status(500).json({ err: e.message });
    }
  });
};
