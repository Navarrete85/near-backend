import { Router, Request, Response } from 'express';
import MenuService from '../../services/MenuService';
import { Container } from 'typedi';
// import { ENUMS, ENUMS_MAP } from '../../definitions/enum-hub';
const route = Router();

export default (app: Router) => {
  app.use('/menu', route);

  route.get('/all', async (req: Request, res: Response) => {
    try {
      let MenuServiceInstance = Container.get(MenuService);
      const allTables = await MenuServiceInstance.getAll();
      return res.status(200).json(allTables);
    } catch (e) {
      return res.status(500).json(e);
    }
  });
};
