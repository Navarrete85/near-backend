import { Router, Request, Response } from 'express';
import OfferService from '../../services/OfferService';
import { Container } from 'typedi';
// import { ENUMS, ENUMS_MAP } from '../../definitions/enum-hub';
const route = Router();

export default (app: Router) => {
  app.use('/offer', route);

  route.get('/all', async (req: Request, res: Response) => {
    try {
      let OfferServiceInstance = Container.get(OfferService);
      const allTables = await OfferServiceInstance.getAll();
      return res.status(200).json(allTables);
    } catch (e) {
      return res.status(500).json(e);
    }
  });
};
