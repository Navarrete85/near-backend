import { Router, Request, Response } from 'express';
import EstablishmentService from '../../services/EstablishmentService';
import { Container } from 'typedi';
// import { ENUMS, ENUMS_MAP } from '../../definitions/enum-hub';
const route = Router();

export default (app: Router) => {
  app.use('/establishment', route);

  route.get('/all', async (req: Request, res: Response) => {
    try {
      let EstablishmentServiceInstance = Container.get(EstablishmentService);
      const allTables = await EstablishmentServiceInstance.getAll();
      return res.status(200).json(allTables);
    } catch (e) {
      return res.status(500).json(e);
    }
  });
};
