import { Router, Request, Response } from 'express';
import AllergenService from '../../services/AllergenService';
import { Container } from 'typedi';
// import { ENUMS, ENUMS_MAP } from '../../definitions/enum-hub';
const route = Router();

export default (app: Router) => {
  app.use('/allergen', route);

  route.get('/all', async (req: Request, res: Response) => {
    try {
      let AllergenServiceInstance = Container.get(AllergenService);
      const allTables = await AllergenServiceInstance.getAll();
      return res.status(200).json(allTables);
    } catch (e) {
      return res.status(500).json(e);
    }
  });
};
