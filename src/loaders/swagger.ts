import express from 'express';
import swaggerJSDoc from 'swagger-jsdoc';
import swaggerUI from 'swagger-ui-express';

export default ({ app }: { app: express.Application }) => {
  const swaggerOptions = {
    swaggerDefinition: {
      info: {
        title: 'Near-Waiter API',
        version: '1.0.0',
      },
    },
    apis: ['src/api/routes/tables.ts'],
  };

  const swaggerDocs = swaggerJSDoc(swaggerOptions);
  app.use('/api-docs', swaggerUI.serve, swaggerUI.setup(swaggerDocs));
};
