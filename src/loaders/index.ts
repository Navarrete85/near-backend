import expressLoader from './express';
import dependencyInjectorLoader from './dependencyInjector';
import mongooseLoader from './mongoose';
import jobsLoader from './jobs';
import Logger from './logger';
import Swagger from './swagger';
//We have to import at least all the events once so they can be triggered
import './events';

export default async ({ expressApp }) => {
  const mongoConnection = await mongooseLoader();
  Logger.info('✌️ DB loaded and connected!');

  /**
   * WTF is going on here?
   *
   * We are injecting the mongoose models into the DI container.
   * I know this is controversial but will provide a lot of flexibility at the time
   * of writing unit tests, just go and check how beautiful they are!
   */

  const userModel = {
    name: 'userModel',
    model: require('../models/user').default,
  };

  const tablesModel = {
    name: 'tablesModel',
    model: require('../models/tables').default,
  };

  const offerModel = {
    name: 'offerModel',
    model: require('../models/OfferModel').default,
  };

  const menuModel = {
    name: 'menuModel',
    model: require('../models/MenuModel').default,
  };

  const establishmentModel = {
    name: 'establishmentModel',
    model: require('../models/EstablishmentModel').default,
  };

  const allergenModel = {
    name: 'allergenModel',
    model: require('../models/AllergenModel').default,
  };

  // It returns the agenda instance because it's needed in the subsequent loaders
  const { agenda } = await dependencyInjectorLoader({
    mongoConnection,
    models: [userModel, tablesModel, offerModel, menuModel, establishmentModel, allergenModel],
  });
  Logger.info('✌️ Dependency Injector loaded');

  await Swagger({ app: expressApp });
  Logger.info('✌️ Swagger loaded');

  await jobsLoader({ agenda });
  Logger.info('✌️ Jobs loaded');

  await expressLoader({ app: expressApp });
  Logger.info('✌️ Express loaded');
};
