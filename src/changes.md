# Cambios necesarios para autenticación.

Como vamos a cambiar la librería que afecta a la autenticación mediante token (express-jwt por jsonwebtoken), tenemos que realizar diversos cambios en los siguientes archivos.

- src/api/middleware/isAuth
- src/loaders/express

Debido al cambio del modelo de datos de usuario, tenemos que realizar cambios en los siguientes archivos:

- src/api/routes/auth.ts --> Cambio para el inicio de sesión, registro y logout.
- src/api/middleware/attachCurrentUser.ts --> Obtención de usuario de la base de datos cuando este esté autorizado.
- src/models/user.ts --> Definición del modelo de datos para conexión con la base de datos Mongodb
- src/services/auth.ts --> Servicios para la autenticación, signin, signUp
- src/services/mailer.ts --> Servicio para el envio de emails
- src/subscribers/user.ts --> Servicios para disparar acciones
- src/types/express/index.d.ts --> Declaración de interfaces globales
