/* eslint-disable  @typescript-eslint/no-unused-vars */
import { Document, Model } from 'mongoose';
/* eslint-disable  @typescript-eslint/no-unused-vars */
import { IUser } from '../../definitions/interfaces/IUser';
/* eslint-disable  @typescript-eslint/no-unused-vars */
import { ITables } from '../../definitions/interfaces/ITable';
/* eslint-disable  @typescript-eslint/no-unused-vars */
import { IOfer } from '../../definitions/interfaces/IOffers';
/* eslint-disable  @typescript-eslint/no-unused-vars */
import { IMenu } from '../../definitions/interfaces/IMenu';
/* eslint-disable  @typescript-eslint/no-unused-vars */
import { IEstablishment } from '../../definitions/interfaces/IEstablishment';
/* eslint-disable  @typescript-eslint/no-unused-vars */
import { IAllergen } from '../../definitions/interfaces/IAllergen';
declare global {
  namespace Express {
    export interface Request {
      currentUser: IUser & Document;
    }
  }

  namespace Models {
    export type UserModel = Model<IUser & Document>;
    export type TablesModel = Model<ITables & Document>;
    export type OfferModel = Model<IOfer & Document>;
    export type MenuModel = Model<IMenu & Document>;
    export type EstablishmentModel = Model<IEstablishment & Document>;
    export type AllergenModel = Model<IAllergen & Document>;
  }
}
