import { ITables } from '../definitions/interfaces/ITable';
// import { ENUMS, ENUMS_MAP } from '../definitions/enum-hub';
import mongoose from 'mongoose';
import { stringLenghtValidator } from '../utils/validator';

const TableSchema = new mongoose.Schema({
  uuid: {
    type: String,
    required: true,
  },
  identifier: {
    type: String,
    required: true,
  },
  order: {
    type: Number,
    required: true,
  },
  state: {
    type: String,
    default: 0,
  },
  startTime: {
    type: Date,
    default: '',
  },
  timeOfStateChange: Date,
});

const EstablishmentSectionSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
  },
  tables: [TableSchema],
});

const TablesSchema = new mongoose.Schema({
  establishmentIid: {
    type: String,
    required: true,
  },
  sections: [EstablishmentSectionSchema],
});

//Definición de las funciones validadoras pertinentes para el modelo de datos.
TableSchema.path('uuid').validate((uuid: string) => {
  stringLenghtValidator(16, 16).validate(uuid);
});

export default mongoose.model<ITables & mongoose.Document>('tables', TablesSchema);
