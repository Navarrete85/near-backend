import { IAllergen } from '../definitions/interfaces/IAllergen';
// import { ENUMS, ENUMS_MAP } from '../definitions/enum-hub';
import mongoose from 'mongoose';

const ImageSchema = new mongoose.Schema({
  _id: mongoose.Schema.Types.ObjectId,
  url: {
    type: String,
    required: true,
  },
});

const AllergenSchema = new mongoose.Schema({
  _id: mongoose.Schema.Types.ObjectId,
  name: {
    type: String,
    required: true,
  },
  image: ImageSchema,
});

//Definición de las funciones validadoras pertinentes para el modelo de datos.

export default mongoose.model<IAllergen & mongoose.Document>('allergens', AllergenSchema);
