import { IOfer } from '../definitions/interfaces/IOffers';
// import { ENUMS, ENUMS_MAP } from '../definitions/enum-hub';
import mongoose from 'mongoose';
import { stringLenghtValidator } from '../utils/validator';

const OfferSchema = new mongoose.Schema({
  _id: mongoose.Schema.Types.ObjectId,
  name: {
    type: String,
    required: true,
  },
  description: String,
  establishmentId: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
  },
});

//Definición de las funciones validadoras pertinentes para el modelo de datos.
OfferSchema.path('name').validate((name: string) => {
  stringLenghtValidator(2, 255).validate(name);
});

export default mongoose.model<IOfer & mongoose.Document>('offers', OfferSchema);
