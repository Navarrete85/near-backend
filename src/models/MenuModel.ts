import { IMenu } from '../definitions/interfaces/IMenu';
// import { ENUMS, ENUMS_MAP } from '../definitions/enum-hub';
import mongoose from 'mongoose';
// import { stringLenghtValidator } from '../utils/validator';

const UrlPdfSchema = new mongoose.Schema({
  _id: mongoose.Schema.Types.ObjectId,
  url: {
    type: String,
    default: '',
  },
});

const ProductCardSchema = new mongoose.Schema({
  _id: mongoose.Schema.Types.ObjectId,
  name: {
    type: String,
    required: true,
  },
  description: {
    type: String,
    required: true,
  },
  price: {
    type: Number,
    required: true,
  },
  allergens: [String],
});

const CartSectionSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
  },
  products: [ProductCardSchema],
});

const CartElementSchema = new mongoose.Schema({
  _id: mongoose.Schema.Types.ObjectId,
  section: [CartSectionSchema],
});

const MenuSchema = new mongoose.Schema({
  _id: mongoose.Schema.Types.ObjectId,
  establishmentId: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
  },
  urlPdf: UrlPdfSchema,
  cart: [CartElementSchema],
});

//Definición de las funciones validadoras pertinentes para el modelo de datos.

export default mongoose.model<IMenu & mongoose.Document>('menus', MenuSchema);
