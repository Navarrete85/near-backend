import { IUser } from '../definitions/interfaces/IUser';
import { ENUMS, ENUMS_MAP } from '../definitions/enum-hub';
import mongoose from 'mongoose';
import { emailValidator, stringLenghtValidator /*passwordValidator  */ /*phoneValidator*/ } from '../utils/validator';

const User = new mongoose.Schema(
  {
    connect: {
      type: Boolean,
      default: false,
    },

    Favorites: [
      {
        type: String,
      },
    ],

    name: {
      type: String,
      required: [true, 'Please enter a first name'],
      index: true,
    },

    email: {
      type: String,
      required: [true, 'Please enter a valid email'],
      lowercase: true,
      unique: true,
    },

    lastName: {
      type: String,
      required: [true, 'Please enter a last name'],
    },

    password: {
      type: String,
      required: [true, 'Please enter a valid password'],
    },

    gender: Number,

    photoUrl: String,

    active: {
      type: Boolean,
      default: false,
    },

    rol: Number,

    addres: String,

    phoneNumber: Number,

    role: {
      type: Number,
      default: ENUMS.ROL.NORMAL,
    },
  },
  { timestamps: true },
);

User.path('email').validate((email: string) => {
  emailValidator.validate(email);
});

User.path('name').validate((name: string) => {
  stringLenghtValidator(3, 30).validate(name);
});

User.path('lastName').validate((lastName: string) => {
  stringLenghtValidator(3, 50).validate(lastName);
});

User.path('password').validate((password: string) => {
  console.error(password);
  return true;
});

User.path('phoneNumber').validate((phoneNumber: number) => {
  console.error(phoneNumber);
  return true;
});

User.path('role').validate((role: number) => {
  return !isNaN(role) && ENUMS_MAP.rolMap[role] !== undefined;
});

User.path('gender').validate((gender: number) => {
  return !isNaN(gender) && ENUMS_MAP.genderMap[gender] !== undefined;
});

export default mongoose.model<IUser & mongoose.Document>('users', User);
