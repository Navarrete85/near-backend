import { IEstablishment } from '../definitions/interfaces/IEstablishment';
// import { ENUMS, ENUMS_MAP } from '../definitions/enum-hub';
import mongoose from 'mongoose';
import { stringLenghtValidator } from '../utils/validator';

const LocationSchema = new mongoose.Schema({
  lat: Number,
  long: Number,
});

const EstablishmentSchema = new mongoose.Schema({
  _id: mongoose.Schema.Types.ObjectId,
  occupation: {
    type: Number,
    default: 0,
  },
  offers: [mongoose.Schema.Types.ObjectId],
  description: {
    type: String,
    required: true,
  },
  imageLogo: {
    type: String,
    required: true,
  },
  location: LocationSchema,
  name: {
    type: String,
    required: true,
  },
  photoUrl: [String],
  uuid: {
    type: String,
    required: true,
  },
});

//Definición de las funciones validadoras pertinentes para el modelo de datos.
EstablishmentSchema.path('uuid').validate((uuid: string) => {
  stringLenghtValidator(16, 16).validate(uuid);
});

export default mongoose.model<IEstablishment & mongoose.Document>('establishments', EstablishmentSchema);
