#nuevo
FROM node:14-alpine

#ARG NODE_ENV=production
ENV APP_ROOT /usr/src/app

RUN apk --no-cache add python make g++

RUN mkdir -p ${APP_ROOT}
RUN npm install typescript -g
WORKDIR ${APP_ROOT}

ADD . ${APP_ROOT}
#VOLUME /usr/src/nuxt-app
#COPY package*.json ./


RUN npm install
RUN npm run build

#ENV NODE_ENV=${NODE_ENV}
# RUN npm run build

#ENV HOST 0.0.0.0
# set app serving to permissive / assigned
#ENV NUXT_HOST=0.0.0.0
# set app port
#ENV NUXT_PORT=3000

#EXPOSE 5000

CMD ["npm", "start"]